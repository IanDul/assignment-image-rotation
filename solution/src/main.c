#include "all_files_actions.h"
#include "bmp_files_actions.h"
#include "bmp_headers.h"
#include "image.h"
#include "transformation.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {

    if (argc != 3) {
        printf("Invalid input\n", stderr);
        return 1;
    }

    FILE* in_file = open_file(argv[1], RB);
    if (in_file == NULL) {
        return 1;
    }

    FILE* out_file = open_file(argv[2], WB);
    struct image image = {0};
    if (out_file == NULL) {
        return 1;
    }

    enum read_status read_s = from_bmp(in_file, &image);
    if (read_s != READ_OK) {
        printf("Something bad with reading of file...\n", stderr);
        return 1;
    }
    else{ printf("File was read\n", stdout); }

    struct image rotated_image = rotate(&image);

    enum write_status write_s = to_bmp(out_file, &rotated_image);
    if (write_s != WRITE_OK) {
        printf("Something bad with writing in file...\n", stderr);
        return 1;
    }
    else{ printf("Data was written in file\n", stdout); }
    
    close_file(in_file);
    close_file(out_file);

    free_image_memory(&image);
    free_image_memory(&rotated_image);

    return 0;
}
