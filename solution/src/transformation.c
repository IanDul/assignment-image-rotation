#include "image.h"
#include <stdlib.h>

struct image rotate( struct image const* source ) {
    struct image new_image = create_image(source->height, source->width);

    size_t index = 0;
    
    for (size_t i = source->height * source->width - source->width; i < source->width * source->height; i++) {
        for (size_t j = 0; j < source->height * source->width; j = j + source->width) {
            new_image.data[index] = source->data[i - j];
            index++;
        }
    }

    return new_image;
}
