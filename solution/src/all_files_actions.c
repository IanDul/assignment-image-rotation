#include "all_files_actions.h"
#include <stdlib.h>

FILE* open_file(const char* filepath, enum open_mode mode) {
    FILE* file = NULL;

    if (mode == RB) {
        file = fopen(filepath, "rb");
    }

    else if (mode == WB) {
        file = fopen(filepath, "wb");
    }

    if (file == NULL) {
        printf("Something bad with openning of file...\n", stderr);
    }
    else {
        printf("File is open\n", stdout);
    }

    return file;
}


void close_file(FILE* file) {
    if (fclose(file) != 0) {
        printf("File isn't closed\n", stderr);       
    }
    else {
        printf("File is closed\n", stdout);
    }
}
