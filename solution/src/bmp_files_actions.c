#include "bmp_headers.h"
#include "bmp_files_actions.h"
#include "image.h"
#include <stdlib.h>

/* padding */
static int64_t calc_padding( const uint32_t width) {
    int64_t padding = 4 - (width * 3) % 4;
    return padding % 4;
}

/* reading */

static int get_image(const struct bmp_header* header, FILE* in, struct image* image) {
    *image = create_image(header->biWidth, header->biHeight);
    int64_t padding = calc_padding(image->width);
    for (size_t i = 0; i < image->height; i++) {
        if (fread(image->data + image->width * i, sizeof(struct pixel), image->width, in) != image->width) {
            printf("Reading of image has gone wrong\n", stderr);
            return 1;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            printf("Reading of image has gone wrong\n", stderr);
            return 2;
        }  
    }

    return 0;
}

static enum read_status header_read_validation(size_t read_exit_code, uint16_t header_bits) {

    if (read_exit_code != 1) {
        printf("Reading of BMP-header has gone wrong\n", stderr);
        return READ_INVALID_HEADER;
    }

    if (header_bits != 24) {
        printf("Reading of BMP-header has gone wrong\n", stderr);
        return READ_INVALID_BITS;
    }

    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    enum read_status read_status = header_read_validation(fread(&header, sizeof(struct bmp_header), 1, in), header.biBitCount);
    if (read_status != READ_OK) {
        return read_status;
    }

    if (get_image(&header, in, img) != 0) {
        free_image_memory(img);
        return READ_ERROR;
    }

    return READ_OK;

}

/* writing */
static enum write_status write_headers(FILE* out, struct image const* img, size_t padding) {
    struct bmp_header headers = {
        .bfType = 0x4d42,
        .bfileSize = img->height * (img->width * sizeof(struct pixel) + padding) + padding + sizeof(struct bmp_header),
        .bfReserved = 0,
        .bOffBits = 54,
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = img->height * (img->width * sizeof(struct pixel) + padding) + padding,
        .biXPelsPerMeter = 2834,
        .biYPelsPerMeter = 2834,
        .biClrUsed = 0,
        .biClrImportant = 0
    };

    if (fwrite(&headers, sizeof(struct bmp_header), 1, out) != 1) {
        printf("Writing of BMP-header has gone wrong\n", stderr);
        return WRITE_HEADER_ERROR;
    }

    return WRITE_OK;
    
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    const int64_t padding = calc_padding(img->width);
    const uint8_t padding_bytes[3] = {0};

    enum write_status write_header_status = write_headers(out, img, padding);
    if (write_header_status != WRITE_OK) {
        return write_header_status;
    }

    for (size_t i = 0; i < img->height; i++) {
        if (img->width != fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out)) {
            printf("Writing of image has gone wrong\n", stderr);
            return WRITE_ERROR;
        }
        if (padding != fwrite(padding_bytes, sizeof(uint8_t), padding, out)) {
            printf("Writing of image has gone wrong\n", stderr);
            return WRITE_ERROR;
        }
    }
    
    return WRITE_OK;
}
