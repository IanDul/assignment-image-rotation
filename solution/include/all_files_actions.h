#ifndef ALL_FILES_ACTIONS
#define ALL_FILES_ACTIONS
#include <stdio.h>

enum open_mode {
  RB,
  WB
};

enum open_status {
    OPEN_ERROR,
    OPEN_OK
};

FILE* open_file( const char* filepath, enum open_mode );

void close_file( FILE* );

#endif
